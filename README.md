# ct_ght
![@DirectPositiveAction](https://hub.ceilotierra.space/photo/443e3c9c-3ab4-4e97-877b-4a0bddd11d0e-1.jpg)

The Gatehouse Terrace, at SkyEarth.
- is an opensourced *reparation project*, in collaboration with [Colectivo CieloTierra](https://hub.ceilotierra.space/@colectivo).

This is a rolling release of works, provided under an **Educational Community License, v2.0**