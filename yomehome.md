located on the East side of the *Gatehouse Terrace* 
- opening onto the **Huerto** (garden) area. 

![ght_e3-291119](https://hub.ceilotierra.space/photo/5e82c6a5-8f5a-4352-8ddb-a1df7a9e75f2-1.jpg)

My *YomeHome* is a wood and canvas structure ~13ft round
It is a hybrid blend of a **Yurt** and a **Geodesic Dome**. It is designed as a temporary living structure and is able to be quickly and easily dismantled and relocated.

- currently comprising, 
    - 7 vertical legs (connected in a V - so 14 in total!)
    - 6 horizontal poles
    - 7 vertical roof poles
    - 2 canvas wall curtain sides
    - 1 canvas roof 
    - 1 canvas roof cover