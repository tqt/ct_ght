@tqt <i>the gatehouse terrace</i> is divided into <i>zoned areas</i>

* East
* West
* 0

<i>subdivisions</i> are then applied for further clarification:
* 1
* 2
* 3

in total this comprises of <u>7 designated areas</u> within <b>the gatehouse terrace</b>

Further project updates: <url>http://z.tqt.solutions/@ght</url>